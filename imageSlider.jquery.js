(function($){
    $.fn.imageSlider = function(imgs) {
        var i = 0;

        $('.slides').html('<img class="rounded img-fluid" src="images/' + imgs[i] + '" alt="' + imgs[i] + '">');

        $('body').on('click', '.slides', function() {
            i++;
            i %= imgs.length;
            $(this).html('<img class="rounded img-fluid" src="images/' + imgs[i] + '" alt="' + imgs[i] + '">');
        });
    };
})(jQuery);